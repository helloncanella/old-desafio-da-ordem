'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

gulp.task('sass', function () {
  return gulp.src('./sass/bundle.scss')
    .pipe($.sassGlob())
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.autoprefixer({
			browsers: ['> 0.5%', 'iOS > 5'],
			cascade: false
		}))
    .pipe(gulp.dest('./css'));
});

gulp.task('jade', function() {
  gulp.src('./jade/**/*.jade')
    .pipe($.jade({
      pretty: true
    }))
    .pipe(gulp.dest('./'));
});

gulp.task('default', ['sass', 'jade'], function () {
  gulp.watch(['./sass/**/*.scss'], ['sass']);
  gulp.watch('./jade/**/*.jade', ['jade']);
});
